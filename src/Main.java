/* Jason Wu
 * 1.12.2015
 */
import util.PriorityQueue;

public class Main {

	public static void main(String[] args) {
		// TODO : Move into a JUnit test - done
		PriorityQueue<String> pq = new PriorityQueue<String>();
		// setup example
		pq.add(4, "4");
		pq.add(1, "1");
		pq.add(3, "3");
		pq.add(2, "2");
		pq.add(1, "1");
		pq.add(2, "2");

		System.out.println(pq.remove()); // 1
		System.out.println(pq.remove()); // 1
		pq.add(1, "1");
		System.out.println(pq.remove()); // 2 - priority burst effect
		System.out.println(pq.remove()); // 1
		System.out.println(pq.remove()); // 2
		System.out.println(pq.remove()); // 3

		pq.add(4, "4");
		pq.add(1, "1");
		pq.add(3, "3");
		pq.add(2, "2");
		pq.add(2, "2");

		System.out.println(pq.remove()); // 1
		System.out.println(pq.remove()); // 2
		pq.add(1, "1");
		System.out.println(pq.remove()); // 1
		System.out.println(pq.remove()); // 2
		System.out.println(pq.remove()); // 3
		System.out.println(pq.remove()); // 4

		pq.add(1, "1");
		pq.add(1, "1");
		pq.add(1, "1");
		pq.add(1, "1");
		pq.add(1, "1");
		pq.add(1, "1");

		pq.add(2, "2");
		pq.add(2, "2");
		pq.add(2, "2");

		pq.add(3, "3");

		System.out.println(pq.remove()); // 1
		System.out.println(pq.remove()); // 1
		System.out.println(pq.remove()); // 2

		System.out.println(pq.remove()); // 1
		System.out.println(pq.remove()); // 1
		System.out.println(pq.remove()); // 2

		System.out.println(pq.remove()); // 1
		System.out.println(pq.remove()); // 1
		System.out.println(pq.remove()); // 3
	}

}
