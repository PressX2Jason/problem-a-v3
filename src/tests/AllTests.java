package tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ Example_1.class, Example_2.class, Example_3.class,}) //Example_4.class })
public class AllTests {

}
