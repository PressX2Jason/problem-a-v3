package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import util.PriorityQueue;

public class Example_1 {	
	@Test
	public void test() {
		PriorityQueue<String> pq = new PriorityQueue<String>();
		pq.add(4, "4");
		pq.add(1, "1");
		pq.add(3, "3");
		pq.add(2, "2");
		pq.add(1, "1");
		pq.add(2, "2");
		
		assertEquals(pq.remove(),"1");	//1
		assertEquals(pq.remove(),"1");	//1
		pq.add(1, "1");
		assertEquals(pq.remove(),"2");	//2 - priority burst effect
		assertEquals(pq.remove(),"1");	//1
		assertEquals(pq.remove(),"2");	//2
		assertEquals(pq.remove(),"3");	//3		
	}

}
