package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import util.PriorityQueue;

public class Example_3 {

	@Test
	public void test() {
		PriorityQueue<String> pq = new PriorityQueue<String>();
		pq.add(1, "1");
		pq.add(1, "1");
		pq.add(1, "1");
		pq.add(1, "1");
		pq.add(1, "1");
		pq.add(1, "1");

		pq.add(2, "2");
		pq.add(2, "2");
		pq.add(2, "2");

		pq.add(3, "3");

		assertEquals(pq.remove(), "1"); // 1
		assertEquals(pq.remove(), "1"); // 1
		assertEquals(pq.remove(), "2"); // 2

		assertEquals(pq.remove(), "1"); // 1
		assertEquals(pq.remove(), "1"); // 1
		assertEquals(pq.remove(), "2"); // 2

		assertEquals(pq.remove(), "1"); // 1
		assertEquals(pq.remove(), "1"); // 1
		assertEquals(pq.remove(), "3"); // 3
	}

}
