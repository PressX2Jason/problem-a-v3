package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import util.PriorityQueue;

public class Example_4 {

	@Test
	public void test() {
		PriorityQueue<String> pq = new PriorityQueue<String>();
		
		//4132 1423 2413 3521 3612 4241 3215 2112 311

		pq.add(4, "4");
		pq.add(1, "1");
		pq.add(3, "3");
		pq.add(2, "2");
		pq.add(1, "1");
		pq.add(4, "4");
		pq.add(2, "2");
		pq.add(3, "3");
		pq.add(2, "2");
		pq.add(4, "4");
		pq.add(1, "1");
		pq.add(3, "3");
		pq.add(3, "3");
		pq.add(5, "5");
		pq.add(2, "2");
		pq.add(1, "1");
		pq.add(3, "3");
		pq.add(6, "6");
		pq.add(1, "1");
		pq.add(2, "2");
		pq.add(4, "4");
		pq.add(2, "2");
		pq.add(4, "4");
		pq.add(1, "1");
		pq.add(3, "3");
		pq.add(2, "2");
		pq.add(1, "1");
		pq.add(5, "5");
		pq.add(2, "2");
		pq.add(1, "1");
		pq.add(1, "1");
		pq.add(2, "2");
		pq.add(3, "3");
		pq.add(1, "1");
		pq.add(1, "1");


		//1 1 2 1 1 2 3 1 1 2 1 1 2 3 4 1 1 2 1 2 3 2 2 3 4 5 2 3 3 4 3 4 5 6 4
		assertEquals(pq.remove(),"1");
		assertEquals(pq.remove(),"1");
		assertEquals(pq.remove(),"2");
		assertEquals(pq.remove(),"1");
		assertEquals(pq.remove(),"1");
		assertEquals(pq.remove(),"2");
		assertEquals(pq.remove(),"3"); //why is the answer 3? there are still 
		assertEquals(pq.remove(),"1");
		assertEquals(pq.remove(),"1");
		assertEquals(pq.remove(),"2");
		assertEquals(pq.remove(),"1");
		assertEquals(pq.remove(),"1");
		assertEquals(pq.remove(),"2");
		assertEquals(pq.remove(),"3");
		assertEquals(pq.remove(),"4");
		assertEquals(pq.remove(),"1");
		assertEquals(pq.remove(),"1");
		assertEquals(pq.remove(),"2");
		assertEquals(pq.remove(),"1");
		assertEquals(pq.remove(),"2");
		assertEquals(pq.remove(),"3");
		assertEquals(pq.remove(),"2");
		assertEquals(pq.remove(),"2");
		assertEquals(pq.remove(),"3");
		assertEquals(pq.remove(),"4");
		assertEquals(pq.remove(),"5");
		assertEquals(pq.remove(),"2");
		assertEquals(pq.remove(),"3");
		assertEquals(pq.remove(),"3");
		assertEquals(pq.remove(),"4");
		assertEquals(pq.remove(),"3");
		assertEquals(pq.remove(),"4");
		assertEquals(pq.remove(),"5");
		assertEquals(pq.remove(),"6");
		assertEquals(pq.remove(),"4");


		
	}

}
