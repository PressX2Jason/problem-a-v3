/* Jason Wu
 * 1.12.2015
 * Class implements a priority queue with the following constraints:
 * Constraint A: Items are only FIFO within their priority class.
 * Constraint B: Items with the highest priority are dequeued first unless the following 
 * condition applies: For every 2 items dequeued with priority "P", the next item must be of 
 * priority "P+1". We call the count �2� as the Burst Rate of a priority class. The two items 
 * dequeued must not necessarily be dequeued consecutively for this constraint to apply. 
 */

package util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.ListIterator;

public class PriorityQueue<T> {

	private ArrayList<Priority<T>>	priorityList;

	public PriorityQueue() {
		priorityList = new ArrayList<Priority<T>>();
	}

	// operations of a queue

	/*
	 * Each queue-item is a combination of priority and value of type T Function attempts to add the item t into the queue,
	 * @returns true if successful, false if otherwise
	 */
	public synchronized boolean add(int p, T t) {
		// find the priority class with priority of level, if it exists
		for (Priority<T> pList : priorityList) {
			if (pList.getPriority() == p) {
				return pList.enqueue(t);
			}
		}
		// otherwise create Priority class with level, add it to the list
		Priority<T> pri = new Priority<T>(p);
		priorityList.add(pri);
		
		// sort the list so it remains in order
		Collections.sort(priorityList, new Comparator<Priority<T>>() {

			@Override
			public int compare(Priority<T> p1, Priority<T> p2) {
				// -1,0,1 for p1 (less then, equal to, greater then) p2
				if (p1.getPriority() < p2.getPriority()) {
					return -1;
				}
				if (p1.getPriority() > p2.getPriority()) {
					return 1;
				}
				return 0;
			}

		});
		// add the item to the queue
		return pri.enqueue(t);
	}

	/*
	 * removes the first element in the queue, following the constraints listed above
	 * @returns true if operation was successful, and t was removed, otherwise null if there is no more elements in the queue
	 */
	public synchronized T remove() {
		// iterate through the list to find the first priority level has not
		// bursted
		ListIterator<Priority<T>> it = priorityList.listIterator();
		boolean popped = false;
		T result = null;

		while (it.hasNext() && !popped) {
			Priority<T> pri = it.next();
			if (pri.getEnqueued() > 0 && !pri.overBurst() ) {
				result = pri.dequeue();
				// go back up the queue to reset the bursts for all [0, p-1] levels
				it.previous(); // move the iterator into the correct position
				while (it.hasPrevious()) {
					pri = it.previous();
					pri.resetBurst();
				}
				popped = true;
			}
		}
		return result;
	}
}
