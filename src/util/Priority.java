/* Jason Wu
 * 1.12.2015
 * Priority class signifies a level of priority, with a queue to ensure FIFO
 */

package util;

import java.util.concurrent.ArrayBlockingQueue;

public class Priority<T> {

	private int	                  priority	     = -1;	  // the priority of this class, with 1 being the highest priority
	private ArrayBlockingQueue<T>	tQueue;	          // the list of elements of type T in this priority class

	private static final int	  BURST_RATE	 = 2;	  // For every $burstRate items dequeued with priority "P", the next item must be of priority "P+1".
	private int	                  numDequeued;	          // the number of elements removed since the last burst, 0 if it has not bursted yet
	private int	                  numEnqueued;	          // the number of elements currently enqueued at this level
	private static final int	  MAX_QUEUE_SIZE	= 50;	 // the maximum number of elements allowed in the queue before it blocks

	public Priority(int priority) {
		this.priority = priority;
		tQueue = new ArrayBlockingQueue<T>(MAX_QUEUE_SIZE);
		numDequeued = 0;
	}

	// adds item t to the tList, items FIFO
	public synchronized boolean enqueue(T t) {
		numEnqueued++;
		return tQueue.add(t);
	}

	/*
	 * Pops the first item in the queue, but does not check for overBursts
	 * @returns the first item inside of the queue
	 */
	public synchronized T dequeue() {
		numEnqueued--;
		numDequeued++;
		return tQueue.poll();
	}

	/*
	 * Check to see if this priority P has exceeded the burst rate
	 * @returns true if P priority has exceeded the burst rate
	 */
	public boolean overBurst() {
		return numDequeued == BURST_RATE;
	}

	/*
	 * Resets the number of removed elements from this level
	 */
	public void resetBurst() {
		numDequeued = 0;
	}

	//setters and getters
	public int getEnqueued() {
		return numEnqueued;
	}

	public int getPriority() {
		return priority;
	}

}
